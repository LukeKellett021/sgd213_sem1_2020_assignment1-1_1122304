﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarchingBytes;

/// <summary>
/// DestroyedOnCollision gives the attached object a collision behaviour, that will destroy the
/// attached object depending on a list of tags, and whether they should be considered, or ignored.
/// </summary>
public class DestroyedOnCollision : DetectCollisionBase
{
    protected override void ProcessCollision(GameObject other) {
        base.ProcessCollision(other);
        EasyObjectPool.instance.ReturnObjectToPool(other); //Return to object pool instead of destroy.
    }
}
