﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

/// <summary>
/// Deals damage to objects with IHealth on collision with the object this script is attached to.
/// It will also destory this object after collision. 
/// </summary>
public class DamageOnCollision : DetectCollisionBase
{
    
    [SerializeField]
    private int damageToDeal;

    protected override void ProcessCollision(GameObject other)
    {
        base.ProcessCollision(other);
        // If the enemy has a IHealth component, deal damage to them.
        if (other.GetComponent<IHealth>() != null) {
            other.GetComponent<IHealth>().TakeDamage(damageToDeal);
           
        }
        else { // Otherwise do nothing.
            Debug.Log(other.name + " does not have an IHealth component");
        }
        // Send object back to pool ("destroy") once damage is dealt to other object.
        switch (tag) //Turns out this switch is pointless, the objects are added to the right pool anyway. I'll still keep it for the Debug.Log though.
                     //Actually the switch is useful for spawning and pulling from pool, not destroying / adding to pool. It is seen in SpawnOverTime.cs
        {
            case "PlayerBullet":
              Debug.Log("PlayerBullet Returned to Pool.");
              EasyObjectPool.instance.ReturnObjectToPool(gameObject);
              break;
            case "EnemyBullet":
              Debug.Log("EnemyBullet Returned to Pool.");
              EasyObjectPool.instance.ReturnObjectToPool(gameObject);
              break;
            case "Pickup":
              Debug.Log("Object to be pooled didn't match a tag");
              break;
        }
        // Destroy(gameObject); Would have otherwise destoryed object here.
    }
}
