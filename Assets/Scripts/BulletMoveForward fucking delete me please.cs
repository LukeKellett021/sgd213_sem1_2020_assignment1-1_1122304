﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Gives movement properties to bullet objects.
/// </summary>
public class BulletMoveForward : MonoBehaviour {

    //An old version of the movement script that, by existing, wasted a few hours of my time.

    // ██████╗░███████╗██╗░░░░░███████╗████████╗███████╗  ███╗░░░███╗███████╗
    // ██╔══██╗██╔════╝██║░░░░░██╔════╝╚══██╔══╝██╔════╝  ████╗░████║██╔════╝
    // ██║░░██║█████╗░░██║░░░░░█████╗░░░░░██║░░░█████╗░░  ██╔████╔██║█████╗░░
    // ██║░░██║██╔══╝░░██║░░░░░██╔══╝░░░░░██║░░░██╔══╝░░  ██║╚██╔╝██║██╔══╝░░
    // ██████╔╝███████╗███████╗███████╗░░░██║░░░███████╗  ██║░╚═╝░██║███████╗
    // ╚═════╝░╚══════╝╚══════╝╚══════╝░░░╚═╝░░░╚══════╝  ╚═╝░░░░░╚═╝╚══════╝


    private float acceleration = 50f;

    // Public getter required for weapon classes that require a reset on initialVelocity. Set is still private.
    public float initialVelocity = 5f;

    private Rigidbody2D ourRigidbody;

    // Use this for initialization
    void OnEnable()
    {
        Debug.Log("OnEnable Called");
        ourRigidbody = GetComponent<Rigidbody2D>();
        ourRigidbody.velocity = Vector2.up * initialVelocity;
    }


    // Update is called once per frame
    void Update()
    {
        //calculating the speed of the object
        Vector2 ForceToAdd = Vector2.up * acceleration * Time.deltaTime;
        //add the speed and direction to the object
        ourRigidbody.AddForce(ForceToAdd);
    }

}