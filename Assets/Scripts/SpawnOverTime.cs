﻿using UnityEngine;
using System.Collections;
using MarchingBytes;

/// <summary>
/// Main class for the spawner. This script spawns the referenced object in an area every set interval.
/// </summary>
public class SpawnOverTime : MonoBehaviour
{

    // Object to spawn
    [SerializeField]
    private GameObject spawnObject;

    // Delay between spawns
    [SerializeField]
    private float spawnDelay = 2f;

    private Renderer ourRenderer;

    // Use this for initialization
    void Start()
    {

        ourRenderer = GetComponent<Renderer>();

        // Stop our Spawner from being visible!
        ourRenderer.enabled = false;

        // Call the given function after spawnDelay seconds, 
        // and then repeatedly call it after spawnDelay seconds.
        InvokeRepeating("Spawn", spawnDelay, spawnDelay);
    }

    void Spawn()
    {
        float x1 = transform.position.x - ourRenderer.bounds.size.x / 2;
        float x2 = transform.position.x + ourRenderer.bounds.size.x / 2;

        // Randomly pick a point within the spawn object
        Vector2 spawnPoint = new Vector2(Random.Range(x1, x2), transform.position.y);

        // Spawn the object at the 'spawnPoint' position
        //Instantiate(spawnObject, spawnPoint, Quaternion.identity);
        switch (spawnObject.name) //This switch pulls the object from the correct pool.
        {
            case "EnemySpaceship":
                Debug.Log("Enemy Added from the Pool.");
                EasyObjectPool.instance.GetObjectFromPool("Enemy", spawnPoint, Quaternion.identity); //this or transform.rotation in third argument
                break;
            case "HPPickup":
                Debug.Log("HPPickup Added from the Pool.");
                EasyObjectPool.instance.GetObjectFromPool("HealthPickup", spawnPoint, Quaternion.identity);
                break;
            case "WeaponPickup":
                Debug.Log("WeaponPickup Added from the Pool.");
                EasyObjectPool.instance.GetObjectFromPool("WeaponPickup", spawnPoint, Quaternion.identity); 
                break; //BossEnemySpaceship
            case "BossEnemySpaceship":
                Debug.Log("BossEnemySpaceship Added from the Pool.");
                EasyObjectPool.instance.GetObjectFromPool("BossEnemy", spawnPoint, Quaternion.identity); 
                break;
            case "asteroid":
                Debug.Log("Asteroid Added from the Pool.");
                EasyObjectPool.instance.GetObjectFromPool("Asteroid", spawnPoint, Quaternion.identity);
                break;
            default:
                Debug.Log("Object to be pooled didn't match a tag: " + spawnObject.name);
                break;


        }
    }
}
