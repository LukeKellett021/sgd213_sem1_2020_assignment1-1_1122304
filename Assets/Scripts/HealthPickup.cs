﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// HealthPickup is an object that restores HP to player objects. 
/// </summary>
public class HealthPickup : MonoBehaviour
{
    [SerializeField]
    public float HpToHeal;

    //only players can pick up object by colliding with them.
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (col.GetComponent<IHealth>() != null)
            {
                GameObject player = col.gameObject;
                
                HandlePlayerPickup(player);
                Destroy(gameObject);
            }
        }
    }

    //only players can pick up object by colliding with them.
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameObject player = col.gameObject;
            HandlePlayerPickup(player);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// HandlePlayerPickup handles all of the actions after a player has been collided with.
    /// It grabs the IWeapon component from the player, transfers all information to a
    /// new IWeapon (based on the provided weaponType).
    /// </summary>
    /// <param name="player"></param>
    private void HandlePlayerPickup(GameObject player)
    {
        // get the playerInput from the player
        var ihealth = player.GetComponent<IHealth>();
        // handle a case where the player doesnt have a PlayerInput
        if (ihealth == null) {
            Debug.LogError("Player doesn't have a IHealth component.");
            return;
        } else {
            // tell the playerInput to SwapWeapon based on our weaponType
            player.GetComponent<IHealth>().Heal((int)HpToHeal);
        }
    }

}