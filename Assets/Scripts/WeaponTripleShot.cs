﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class WeaponTripleShot : WeaponBase {
    
    /// <summary>
    /// Shoot will spawn a three bullets, provided enough time has passed compared to our fireDelay.
    /// </summary>
    public override void Shoot() {
        // get the current time
        float currentTime = Time.time;
        // if enough time has passed since our last shot compared to our fireDelay, spawn our bullet
        if (currentTime - lastFiredTime > fireDelay) {
            // create 3 bullets
            for (int i = 0; i < 3; i++) {
                GameObject newBullet;
                //changes firing direction depending on whether the player or enemy is firing the bullet. 
                int y = 1;
                if (tag == "Enemy")
                {
                    //If bullet is from an enemy, pull bullet from enemy bullet pool.
                    newBullet = EasyObjectPool.instance.GetObjectFromPool("EnemyBullets", bulletSpawnPoint.position, transform.rotation);
                    //flips firing direction if bullet is from the enemy.
                    y = -1;
                }
                else
                {
                    //If bullet is from an enemy, pull bullet from play bullet pool.
                    newBullet = EasyObjectPool.instance.GetObjectFromPool("PlayerBullets", bulletSpawnPoint.position, transform.rotation);

                    //The problem regarding slowed down bullets are being pulled from a pool, was in a in MoveConstantly.cs, not here
                    //newBullet.GetComponent<Rigidbody2D>().velocity = Vector2.up * newBullet.GetComponent<BulletMoveForward>().initialVelocity;
                }
                // set bullet direction
                newBullet.GetComponent<MoveConstantly>().Direction = new Vector2(-0.5f + 0.5f * i, y * 0.5f);
            }
            // update our shooting state
            lastFiredTime = currentTime;
        }
    }
}