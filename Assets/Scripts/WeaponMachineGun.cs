﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class WeaponMachineGun : WeaponBase {

    /// <summary>
    /// Shoot will spawn a new bullet, provided enough time has passed compared to our fireDelay.
    /// </summary>
    public override void Shoot() {
        // get the current time
        float currentTime = Time.time;
        
        // if enough time has passed since our last shot compared to our fireDelay, spawn our bullet
        if (currentTime - lastFiredTime > fireDelay) {
            GameObject newBullet;
            if (tag == "Enemy")
            {
                //If bullet is from an enemy, pull bullet from enemy bullet pool.
                newBullet = EasyObjectPool.instance.GetObjectFromPool("EnemyBullets", bulletSpawnPoint.position, transform.rotation);
            }
            else
            {
                //pull bullet from player bullet pool.
                newBullet = EasyObjectPool.instance.GetObjectFromPool("PlayerBullets", bulletSpawnPoint.position, transform.rotation);
                
            }
            // update our shooting state
            lastFiredTime = currentTime;
        }
    }
}
