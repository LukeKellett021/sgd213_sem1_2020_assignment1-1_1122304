﻿using UnityEngine;
using System.Collections;

public class EnemyMoveForward : MonoBehaviour 
{
    private float acceleration = 75f;

    private float initialVelocity = 2f;

    private Rigidbody2D ourRigidbody;
    //
    [SerializeField]
    private float maximumSpinSpeed = 200;

    // Use this for initialization
    void Start()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();
        ourRigidbody.velocity = Vector2.down * initialVelocity;

        //Sends the object rotating at a random angle when created.
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-maximumSpinSpeed, maximumSpinSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        // The speed of the object.
        Vector2 ForceToAdd = Vector2.down * acceleration * Time.deltaTime;

        ourRigidbody.AddForce(ForceToAdd);
    }
}
